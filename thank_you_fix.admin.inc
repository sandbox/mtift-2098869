<?php

/*
 * Create the thank you fix form.
 */
function fix_the_data() {
  $form = array();
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['description'] = array(
    '#markup' => "This program replaces any premium value (in the selected column)
    of $10.20 or less with $0",
  );
  $form['file'] = array(
    '#title' => t('Upload thank you file'),
    '#type'  => 'file',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Fix the File',
  );
  return $form;
}

/*
 * Process the thank you fix form.
 */
function fix_the_data_submit($form, &$form_state) {

  // TODO: Make this configurable on the form
  $fair_market_value = 10.21;
  $market_value_field = 'MarketValue';
  $separator = "|";

  $validators = array('file_validate_extensions' => array('txt'));
  $file = file_save_upload('file', $validators, 'temporary://');
  $handle = fopen($file->uri, 'r');

  // Create a new file
  $new_uri = 'temporary://' . substr($file->filename, 0, -4) . '_fixed.txt';
  $new_file = file_copy($file, $new_uri, FILE_EXISTS_REPLACE);
  // Open the new file for writing as an empty file
  $new_handle = fopen($new_file->uri, 'w+');

  $firsttime = TRUE;
  while ($row = fgetcsv($handle, 0, $delimiter = '|')) {

    // Get header fields
    if ($firsttime == TRUE) {
      $columns = array();
      foreach ($row as $i => $header) {
        $columns[$i] = trim($header);
      }
      // Add the column headers to the new file
      fputcsv($new_handle, $columns, '|');
      $firsttime = FALSE;
      continue;
    }

    // Loop through all of the records in the file, row by row
    $data = '';
    foreach ($row as $i => $field) {

      // Add some variables to avoid confusion
      $column_header = $columns[$i];
      $field_value = $field;

      // Only check user-configured market value data
      if ($column_header == $market_value_field) {
        if ($field_value <> 0 && $field_value < $fair_market_value) {
          $field_value = 0;
        }
      }
      // The first value in the row does not need a separator
      if ($i == 0) {
        $data .= str_replace('"', '', $field_value);
      }
      // Every other value needs a separator + a value
      else {
        $data .= $separator . str_replace('"', '', $field_value);
      }
    }
    $data .= "\n";
    fwrite($new_handle, $data);
  }
  fclose($new_handle);

  $http_headers = module_invoke('file', 'file_download', $new_file->uri);
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . $new_file->filename);
  // Fix problem with IE downloads
  if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
    $http_headers['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0';
    $http_headers['Pragma'] = 'public';
  }
  else {
    $http_headers['Pragma'] = 'no-cache';
  }

  // Download the file
  file_transfer($new_file->uri, $http_headers);
}


